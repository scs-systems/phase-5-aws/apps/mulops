FROM centos:7
RUN yum -y install httpd perl && \
    rm -f /etc/httpd/conf.d/welcome.conf

COPY mulops /var/www/cgi-bin/mulops
COPY webfiles/mulops_http.conf /etc/httpd/conf.d/
COPY webfiles/index.html /var/www/html
RUN ln -s /var/www/cgi-bin/mulops/catalogs /var/www/html
RUN chmod 755 /var/www/cgi-bin/mulops/bin/mulops.cgi

CMD /usr/sbin/httpd -DFOREGROUND